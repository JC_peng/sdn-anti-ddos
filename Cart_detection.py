import pandas as pd
import joblib
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score

if __name__ == "__main__":

    data = pd.read_excel("Scenario I/data/flow_feature_low_12s.xlsx")
    model = joblib.load(filename="Cart_model.m")
    y_true = data.iloc[:, -1]
    y_pred = model.predict(data.iloc[:, :-1])

    tp = sum(y_true & y_pred)
    fp = sum((y_true == 0) & (y_pred == 1))
    tn = sum((y_true == 0) & (y_pred == 0))
    fn = sum((y_true == 1) & (y_pred == 0))

    tpr = tp / (tp + fn)
    fpr = fp / (fp + tn)
    tnr = tn / (fp + tn)
    fnr = fn / (tp + fn)
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1_score = f1_score(y_true, y_pred)

    print("Accuracy: " + str(accuracy))
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1 Score: " + str(f1_score))


