import math
import time
import random


PARAMETER_A = 1 / 300  # Flow table entry process factor calculation parameters
PARAMETER_B = 29 / 300  # Flow table entry process factor calculation parameters
MIN_TEST_INTERVAL = 10  # Minimum detection interval for flow entries, unit is S
MAX_TEST_INTERVAL = 300  # Maximum detection interval for flow entries, unit is S
IDLE_TIMEOUT = 60  # The default value of the flow entry idle timeout, the unit is S
HARD_TIMEOUT = 300  # The default value of the hard timeout of the flow entry, the unit is S
WINDOW_SIZE = 3  # Moving average algorithm sliding window size


class FlowEntryExist(object):

    def __init__(self):
        """
        Initialization function, used to initialize
        flow table information
        """
        self.flow_entry_dic = {}  # Flow entry storage dictionary
        self.test_period_dic = {}  # Dictionary of flow entry detection cycles

    def calculate_all_FE_time(self):
        self.flow_entry_dic = self.flow_info_mgmt()
        for flow_entry in self.flow_entry_dic:
            self.test_period_dic['flow_id']['test_time'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            self.test_period_dic['flow_id']['detection_times'] += 1
            self.test_period_dic['flow_id']['test_result'].append(self.calculate_test_result(flow_entry))
            self.test_period_dic['flow_id']['test_interval'] = self.calculate_time_for_eachFE(flow_entry)

    def calculate_time_for_eachFE(self, flow_entry):
        """
        Calculate the flow entry detection period function,
        used to calculate the flow entry detection period
        """
        process_time = self.calculate_process_factor(flow_entry['detection_times'])
        if flow_entry['test_interval'][-1] == 0:
            return int(process_time / 2)
        trust_time = flow_entry['test_interval'][-1] * self.calculate_trust_factor(flow_entry['test_result'])
        return int((process_time + trust_time) / 2)

    def calculate_process_factor(self, n):
        """
        Calculate the flow entry detection cycle process factor
        function and return the flow entry detection cycle
        process factor
        """
        process_factor = 1 / (PARAMETER_A + PARAMETER_B * math.exp(-n))
        return process_factor

    def calculate_test_result(self, flow_entry):
        """
        Collect discrete values of flow entry detection results
        and calculate trust degree
        """
        result_dic = self.detected(flow_entry)
        test_result = sum(result_dic['test_result'])
        return test_result

    def calc_next_s(self, alpha, s):

        s2 = [0 for i in range(len(s))]

        s2[0] = sum(s[0:3]) / float(3)
        for i in range(1, len(s2)):

            s2[i] = alpha * s[i] + (1 - alpha) * s2[i - 1]

        return s2

    def calculate_trust_factor(self, result_dic):
        """
        Calculate the trust factor factor of the
        flow entry detection period,
        used to calculate the trust factor of the
        flow entry detection period
        """
        alpha = 0.5
        s1 = calc_next_s(alpha, result_dic)

        s2 = calc_next_s(alpha, s1)
        a2 = [(2 * s1[i] - s2[i]) for i in range(len(s1))]
        b2 = [(alpha / (1 - alpha) * (s2[i] - s1[i])) for i in range(len(s1))]

        s_double = [0 for i in range(len(s2))]
        for i in range(1, len(s2)):
            s_double[i] = a2[i - 1] + b2[i - 1] * 1

        predict_double = [0 for i in range(2)]
        for i in range(len(predict_double)):
            predict_double[i] = a2[-1] + b2[-1] * (i + 1)

        s_double.extend(predict_double)
        trust_factor = s_double[-1]

        # Mt_1 = []
        # Mt_2 = []
        # T = len(result_dic)
        # if T < WINDOW_SIZE:
        #     return 0
        # for i in range(0, WINDOW_SIZE):
        #     Mt_1.append(0)
        #     Mt_2.append(0)
        #     for j in range(T - WINDOW_SIZE - 1, T - 1):
        #         Mt_1[i] += result_dic[j] / WINDOW_SIZE
        #     Mt_2[i] += Mt_1[i] / WINDOW_SIZE
        # At = 2 * Mt_2[WINDOW_SIZE-1] - Mt_1[WINDOW_SIZE-1]
        # Bt = 2 * (Mt_1[WINDOW_SIZE-1] - Mt_2[WINDOW_SIZE-1]) / (WINDOW_SIZE - 1)
        # trust_factor = At + Bt * T

        return trust_factor

    def detected(self, flow_entry):
        """
        Extract raw data of test results
        """
        flow_id = flow_entry['flow_id']
        result_dic = {'flow_id': flow_id, 'test_result': [random.randint(0, 1),
                                                          random.randint(0, 1), random.randint(0, 1)]}
        return result_dic


def calc_next_s(alpha, s):

    s2 = [0 for i in range(len(s))]

    s2[0] = sum(s[0:3]) / float(3)
    for i in range(1, len(s2)):
        s2[i] = alpha * s[i] + (1 - alpha) * s2[i - 1]

    return s2
